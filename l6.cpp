/************************
 * Practice: STL        *
 * Date:     2021-03-31 *
 ************************/

// Task 1
// Generate a vector of random integers in the range [min, max]
// Declaration: std::vector<T> randint(int sz, T min, T max);

// Task 2
// Generate a vector of random reals in the range [min, max)
// Declaration: std::vector<T> randreal(int sz, T min, T max);

// Task 3
// Find the minimal and the maximal value in the vector

// Task 4
// struct Person {char name[30], unsigned short age};
// (a) Sort vector<Person> by age in descending order
// (b) Count people whose names start with D

// Task 5
// Translate text to Pig Latin
// Reverse the word and add 'ay'
// "the quick" -> "ehtay kciuqay"

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <algorithm>
#include <string>

template<typename T>
std::vector<T> randint(int sz, T min, T max) {
	std::vector<T> v(sz);
	for (auto& e : v)
		e = std::rand() % (max - min + 1) + min; // [min, max]
	return v;
}

template<typename T>
std::vector<T> randreal(int sz, T min, T max) {
	std::vector<T> v(sz);
	for (auto& e : v)
		e = ((1.*std::rand()-1) / RAND_MAX) * (max - min) + min; // [min, max)
	return v;
}

struct Person {
	char name[30];
	unsigned short age;
};

int main(int argc, const char* argv[]) {
	std::srand(std::time(0));

	// Task 1
	auto vs = randint<short>(10000, -1000, 1000);
	// for (auto i = 0; i < 10; ++i)
	// 	std::cout << vs[i] << std::endl;

	// Task 2
	auto vf = randreal<float>(100000, -0.8, 0.8);
	// for (auto i = 0; i < 10; ++i)
	// 	std::cout << vf[i] << std::endl;

	// Task 3
	auto minmax = std::minmax_element(vf.begin(), vf.end());
	std::cout << "randreal<float>(100000, -0.8, 0.8)\n";
	std::cout << u8"\te ∈ [" << *minmax.first << ", " << *minmax.second << "]\n\n";

	// Task 4
	std::vector<Person> people {
		Person{"Doe, Jane", 25},
		Person{"Einstein, Albert", 48},
		Person{"Sinatra, Frank", 35},
		Person{"Dumbledor, Albus", 120},
		Person{"Blair, Tony", 50}
	};

	std::sort(people.begin(), people.end(), [](auto& a, auto& b){
		return a.age > b.age; // true if a goes before b
	});
	std::cout << "People sorted by age in descending order:\n";
	for (auto& e : people)
		std::cout << '\t' << e.name << " (" << e.age <<")\n";
	std::cout << std::endl;

	int countD = std::count_if(people.begin(), people.end(), [](auto& a){
		return a.name[0] == 'D';
	});
	std::cout << "There are " << countD << " people whose names start with 'D'\n";

	// Task 5
	std::string text{"That which we call a rose by any other name would smell as sweet."};
	size_t beginWord = 0;
	bool isInWord = false;

	std::cout << "\n[ORIGINAL]: " << text << std::endl;
	for (size_t i = 0; i < text.size(); ++i) {
		if (isInWord && !std::isalpha(text[i])) {
			isInWord = false;
			text.insert(i, "ay");
			std::reverse(&text[beginWord], &text[i]);
			i += 2;
		} else if (!isInWord && std::isalpha(text[i])) {
			isInWord = true;
			beginWord = i;
		}
	}
	std::cout << "[PIGLATIN]: " << text << std::endl;
	return 0;
}
