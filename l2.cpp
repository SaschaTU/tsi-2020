/*************************
 * Practice: STRINGS (2) *
 * Date:     2021-03-03  *
 ************************/

#include <cstdio>
#include <cstring>
#include <cctype>

#include <string>
#include <iostream>

int main(int argc, char const *argv[]) {

	printf("Input string: ");
	char str[256];
	fgets(str, 256, stdin); // cin.readline()

	// Task 3
	// User inputs a string (possibly with spaces)
	// User inputs another string (no spaces)
	// Check whether the second string is the substring of the first one

	printf("Input string 2: ");
	char str2[256];
	scanf("%s", str2);
	if (strstr(str, str2)) // char* strstr(const char* haystack, const char* needle)
		printf("String 2 is a substring of string 1.\n");
	else
		printf("String 2 is NOT a substring of string 1.\n");

	// Task 4
	// User inputs a string (possibly with spaces)
	// Remove all the digits from the string

	char str3[256];
	int j3 = 0;
	for (auto i = 0; str[i]; ++i, ++j3) {
		if (isdigit(str[i])) {
			--j3;
			continue;
		} else {
			str3[j3] = str[i];
		}
	}
	str3[j3] = 0;
	printf("[Task 4]: %s\n", str3);

	// Task 5
	// User inputs a string (possibly with spaces)
	// Remove all substrings "ab" from the string

	char str4[256];
	int j4 = 0;
	for (auto i = 0; str[i]; ++i, ++j4) {
		if (str[i] == 'a' && str[i+1] == 'b') {
			--j4;
			++i;
		} else {
			str4[j4] = str[i];
		}
	}
	str4[j4] = 0;
	printf("[Task 5]: %s\n", str4);

	// Task 6
	// User inputs a string (possibly with spaces)
	// Capitalise each word of the string

	char str5[256];
	strcpy(str5, str); // where-what
	for (auto i = 0; str5[i]; ++i) {
		if (i == 0 || str5[i-1] == ' ')
			str5[i] = toupper(str5[i]);
	}
	printf("[Task 6]: %s\n", str5);

	// Task 7
	// Repeat task five using std::string

	std::string sstr {str};
	while (sstr.find("ab") != std::string::npos)
		sstr.erase(sstr.find("ab"), 2);
	std::cout << sstr << std::endl;

	return 0;
}

