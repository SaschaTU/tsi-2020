/********************************
 * Practice: USER-DEFINED TYPES *
 * Date:     2021-03-17         *
 ********************************/

#include <cstring>
#include <cmath>
#include <iostream>
#include <string>

// One example of using C-style strings in functions

// Task 0
// Reverse each C-style string in the array
// Declaration: void reverse(char* arr[], int len);
void reverse(char* arr[], int len) {
	for (int i = 0; i < len; ++i) {
		int l = strlen(arr[i]);
		for (int j = 0; j < l/2; ++j) { // 5/2 = 2: AO|E|UH
			auto temp = arr[i][j];
			arr[i][j] = arr[i][l-j-1];
			arr[i][l-j-1] = temp;
		}
	}
}

// Structures

enum class Department {Board, HR, PR, RnD, Sales};

// Task 1
// Implement a structure for company staff
// -- First name
// -- Last name
// -- Department (Board, HR, PR, RnD, Sales)
// -- Position
// -- Age
// Create several staff members on the heap
// Determine the mean age of the Board

struct Member {
	std::string fname;
	std::string lname;
	Department dpt;
	std::string pos;
	int age;
};

// Task 2
// Implement a structure for complex numbers
// -- Operator +
// -- Methods: abs, arg
// -- std::cout << z

struct Complex {
	double re;
	double im;
	double abs() {return std::sqrt(re*re + im*im);}
	double arg() {return std::atan2(re, im);}
};

Complex operator+(const Complex& z1, const Complex& z2) {
	return Complex{z1.re + z2.re, z1.im + z2.im};
};

std::ostream& operator<<(std::ostream& os, const Complex& z) {
	return os << z.re << (z.im > 0 ? "+" : "") << z.im << "i";
}

int main(int argc, char const *argv[]) {
	// Task 0
	char a[]{"One"};
	char b[]{"Two"};
	char c[]{"Three"};
	char* arr[]{a, b, c};
	reverse(arr, 3);
	std::cout << "[Task 1]\n";
	for (int i = 0; i < 3; ++i)
		std::cout << arr[i] << std::endl;

	// Task 1
	Member* ma = new Member[3]{
		{"John", "Doe", Department::Board, "CEO", 55},
		{"Jane", "Doe", Department::RnD, "Senior developer", 35},
		{"Jack", "Doe", Department::Board, "CFO", 44}
	};

	float meanAge = 0.;
	int countBoard = 0;
	for (int i = 0; i < 3; ++i) {
		if (ma[i].dpt == Department::Board) {
			meanAge += ma[i].age;
			++countBoard;
		}
	}
	std::cout << "\n[Task 2]\n";
	std::cout << "Mean age for the board is "
	          << meanAge/countBoard << std::endl;

	// Task 2
	Complex z{1.1, -2.5};
	Complex x{-0.2, 1.5};
	z + x;
	std::cout << "\n[Task 3]\n";
	std::cout << "abs(" << z << ") = " << z.abs() << std::endl;
	std::cout << "arg(" << z << ") = " << z.arg() << std::endl;

	return 0;
}
