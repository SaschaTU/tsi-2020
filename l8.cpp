/************************
 * Practice: MENU       *
 * Date:     2021-04-07 *
 ************************/

// Task 1
// Implement a menu
// Database
// |- New
// |- Open
// Add
// Sort
// Print

// Task 2
// Draw a colour table (in certain terminals)

#include <iostream>
#include <string>

enum Menu : char {
	menuDatabase = 'd',
	menuDatabaseNew = 'n',
	menuDatabaseOpen = 'o',
	menuAdd = 'a',
	menuSort = 's',
	menuPrint = 'p',
	menuQuit = 'q'
};

void print(const char* str) {
	std::cout << str << std::endl;
	return;
}

bool drawMenuDatabase() {
	std::cout << "\n===============================================\n"
	          << "/Database >"
	          << " \033[7mN\033[mew "
	          << "| \033[7mO\033[mpen "
	          << "| \033[7mQ\033[muit "
	          << "\n================================================\n"
	          << "> ";
	std::string sel;
	std::getline(std::cin, sel);
	switch (std::tolower(sel[0])) {
	case Menu::menuDatabaseNew:
		print("/Database/New");
		break;
	case Menu::menuDatabaseOpen:
		print("/Database/Open");
		break;
	case Menu::menuQuit:
		return 0;
	default:
		std::cout << "(EE) Entry not recognised\n";
	}
	return 1;
}

bool drawMenu() {
	std::cout << "\n================================================\n"
	          << "/ >"
	          << " \033[7mD\033[matabase "
	          << "| \033[7mA\033[mdd "
	          << "| \033[7mS\033[mort "
	          << "| \033[7mP\033[mrint "
	          << "| \033[7mQ\033[muit"
	          << "\n================================================\n"
	          << "> ";
	std::string sel;
	std::getline(std::cin, sel);
	switch (std::tolower(sel[0])) {
	case Menu::menuDatabase:
		print("/Database");
		while(drawMenuDatabase());
		break;
	case Menu::menuAdd:
		print("/Add");
		break;
	case Menu::menuSort:
		print("/Sort");
		break;
	case Menu::menuPrint:
		print("/Print");
		break;
	case Menu::menuQuit:
		std::cout << "Bye\n";
		return 0;
	default:
		std::cout << "(EE) Entry not recognised\n";
	}
	return 1;
}

void drawColours() {
	for (int i = 30; i < 38; ++i) {
		for (int j = 40; j < 48; ++j) {
			std::cout << "\033[" << i << ";" << j << "m " << i << "," << j << " \033[m";
		}
		std::cout << std::endl;
	}
	std::cout << "Regular    "
	          << "\033[1mBold\033[m    "
	          << "\033[3mItalic\033[m    "
	          << "\033[4mUnderline\033[m\n";
}

int main(int argc, char* argv[]) {
	drawColours();
	while (drawMenu());
	return 0;
}
