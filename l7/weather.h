#ifndef WEATHER_H
#define WEATHER_H

#include <vector>
#include <ctime>

struct Weather {
	unsigned int locationId;
	std::time_t timestamp;
	float temperature;
	unsigned short pressure;
	float speed;
	unsigned short direction;
};

struct Location {
	unsigned int id;
	char location[20];
	char country[3];
	float lattitude;
	float longitude;
};

template<typename T>
std::vector<T> readDatabase(const char* fName);

float avgTemperature(std::vector<Weather>& weather, std::vector<Location>& locations, float lattitude);

void printWeather(std::vector<Weather> weather, std::vector<Location> locations);

#endif // WEATHER_H
