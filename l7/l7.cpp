/*****************************
 * Practice: SEVERAL SOURCES *
 * Date:     2021-04-07      *
 *****************************/

// Task 1
// Read in the two relations from files
// Find the average temperature to the North of 58°
// Print out information about the weather (see example):
//     Weather in London, GB
//     Sun Jan 19 05:14:07 2038
//     -2.5 °C | 990 hPa | 1.5 m/s @ 270°

// Task 2
// Move all the declarations to the separate header
// Move all the definitions to the separate source file
// Describe the build process (Makefile/...)

#include <iostream>
#include <vector>

#include "weather.h"
#include "readDatabase.h"

int main(int argc, char* argv[]) {
	auto weather = readDatabase<Weather>("weather.db");
	auto locations = readDatabase<Location>("locations.db");

	float avgTemp = avgTemperature(weather, locations, 45.);
	std::cout << "Average temperature to the North of 45° is " << avgTemp << " °C\n\n";

	printWeather(weather, locations);

	return 0;
}