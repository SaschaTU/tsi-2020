#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm>

#include "weather.h"

float avgTemperature(std::vector<Weather>& weather, std::vector<Location>& locations, float lattitude) {
	float avgTemp = 0.;
	int countTemp = 0;

	for (auto& w : weather) {
		if (std::find_if(locations.begin(), locations.end(), [&](auto& a) {
			return a.id == w.locationId && a.lattitude > lattitude;
		}) != locations.end()) {
			avgTemp += w.temperature;
			++countTemp;
		}
	}
	return avgTemp/countTemp;
}

void printWeather(std::vector<Weather> weather, std::vector<Location> locations) {
	for (auto& w : weather) {
		auto ll = locations.begin();
		if ((ll = std::find_if(locations.begin(), locations.end(), [&](auto& a) {
			return a.id == w.locationId;
		})) != locations.end()) {
			std::cout << "Weather in " << ll->location << ", " << ll->country << std::endl
			          << std::ctime(&w.timestamp)
			          << w.temperature << " °C | "
			          << w.pressure << " hPa | "
			          << w.speed << " m/s @ " << w.direction << "°\n\n";
		}
	}
}
