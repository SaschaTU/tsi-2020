#ifndef READDATABASE_H
#define READDATABASE_H

#include <fstream>
#include <iostream>
#include <vector>

#include "weather.h"

template<typename T>
std::vector<T> readDatabase(const char* fName) {
	std::vector<T> out;
	if (std::ifstream is{fName, std::ios::binary|std::ios::ate}) {
		auto flen = is.tellg();
		auto size = flen / sizeof(T);
		is.seekg(0);
		for (int i = 0; i < size; ++i) {
			T tmp;
			is.read(reinterpret_cast<char*>(&tmp), sizeof tmp);
			out.push_back(tmp);
		}
	} else {
		std::cerr << "File " << fName << " does not exist.\n";
	}
	return out;
}

#endif
