/*************************
 * Practice: STRINGS (1) *
 * Date:     2021-02-24  *
 ************************/

#include <cstdio>
#include <cctype>

int main() {
	char inString[256];
	printf("Input string: ");
	fgets(inString, 256, stdin); // Get up to _256_ chars
	                             // from the stream _stdin_ (user input)
	                             // and store it in _inString_ variable
	
	// Task 1
	// User inputs a string (possibly with spaces)
	// Count lowercase and uppercase letters in it.
	int countLower = 0;
	int countUpper = 0;
	for (auto i = 0; inString[i]; ++i) {
		if (islower(inString[i]))
			++countLower;
		else if (isupper(inString[i]))
			++countUpper;
	}

	// Task 2
	// User inputs a string (possibly with spaces).
	// A space is added after each 'a' character.
	char outString[256];
	int iOutput = 0;
	for (auto iInput = 0; inString[iInput]; ++iInput, ++iOutput) {
		outString[iOutput] = inString[iInput];
		if (inString[iInput] == 'a') {
			outString[++iOutput] = ' ';
		}
	}
	outString[iOutput] = 0;

	printf("Task One: %d uppercase, %d lowercase\n", countUpper, countLower);
	printf("Task Two: %s\n", outString);

	return 0;
}
