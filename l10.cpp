/************************
 * Practice: EXCEPTIONS *
 * Date:     2021-04-19 *
 ************************/

// Task
// Use exceptions to validate the input:
// -- Id should be integer
// -- Code should start with 'M'
// Print out the error message on failed validation

#include <iostream>
#include <string>
#include <limits>

struct Data {
	int id;
	std::string code;
};

struct ValidatorException {
	std::string message;
};

int fillId() {
	int id;
	std::cin.exceptions(std::ios_base::failbit);
	try {
		std::cout << "Id: ";
		std::cin >> id;
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	} catch (const std::exception& e) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		throw ValidatorException{"EEEE Integer expected"};
	}
	return id;
}

std::string fillCode() {
	std::string code;
	std::cout << "Code: ";
	std::cin >> code;
	if (code[0] != 'M')
		throw ValidatorException{"EEEE Code should start with 'M'"};
	return code;
}

int main(int argc, char* argv[]) {
	Data d;
	while (true) {
		try {
			d.id = fillId();
			d.code = fillCode();
			std::cout << "{\"Id\": " << d.id << ", \"Code\": \"" << d.code << "\"}\n";
			break;
		} catch (const ValidatorException& e) {
			std::cout << e.message << std::endl;
		}
	}
	return 0;
}