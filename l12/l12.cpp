/************************
 * Practice: TESTING    *
 * Date:     2021-05-05 *
 ************************/

// Task
// Write test cases for the given functions
// Frameworks [https://en.wikipedia.org/wiki/List_of_unit_testing_frameworks#C++]

#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

using namespace Catch::Matchers;

unsigned int factorial(unsigned int n) {
	return n <= 1 ? 1 : n * factorial(n-1);
}

TEST_CASE("Computing factorials", "[factorial]") {
	REQUIRE(factorial(1) == 1);
	REQUIRE(factorial(10) == 3628800);
	REQUIRE(factorial(0) == 1);
}

double pwr(double x, unsigned int n) {
	return n == 0 ? 1. : x * pwr(x, n-1);
}

TEST_CASE("Computing powers", "[pow]") {
	REQUIRE(pwr(2., 2) == 4.);
	REQUIRE(pwr(-2., 3) == -8.);
	REQUIRE(pwr(0., 4) == 0.);
	REQUIRE(pwr(2., 0) == 1.);
	REQUIRE(pwr(-2., 0) == 1.);
}

double abst(double x) {
	return x >= 0 ? x : -x;
}

TEST_CASE("Computing absolute values", "[abs]") {
	REQUIRE(abst(2.) == 2.);
	REQUIRE(abst(-3.) == 3.);
	REQUIRE(abst(0.) == 0.);
}

double expt(double x, double eps=1.e-4) {
	double curResult = 1;
	double prevResult;
	unsigned int n = 1;
	do {
		prevResult = curResult;
		curResult += pwr(x, n)/factorial(n++);
	} while (abst(curResult-prevResult) > eps);
	return curResult;
}

TEST_CASE("Compute exponent", "[exp]") {
	REQUIRE(expt(0) == 1.);
	REQUIRE_THAT(expt(1.), WithinAbs(2.718281828459045, 1.e-4));
	REQUIRE_THAT(expt(-1.), WithinAbs(0.36787944117, 1.e-4));
	REQUIRE_THAT(expt(-1., 1.e-9), WithinAbs(0.36787944117, 1.e-8));
}