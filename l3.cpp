/************************
 * Practice: FUNCTIONS  *
 * Date:     2021-03-09 *
 ************************/

#include <iostream>
#include <string>

// Possibly a bit more challenging tasks first

// Task C1
// Calculate n! = 1 × 2 × 3 × ... × n
// Restriction: return statement only
// Declaration: unsigned int fact(unsigned int n);
// Example: fact(5) => 120

// Task C2
// Calculate n-th Fibonacci number (1, 1, 2, 3, 5, 8, 13, ...)
// Restriction: return statement only
// Declaration: unsigned int fib(unsigned int n);
// Example: fib(8) => 21

// Task C3
// Given a vector<double>, calculate the sum of squares of its elements
// Restriction: return statement only
// Declaration: double vecProd(const std::vector<double> &v);
// Example: vecProd(std::vector<double>{1, 2, 3}) => 14

// Task C4
// Given a vector<int>, construct a number whose binary digits are the elements of the vector
// Restriction: return statement only
// Declaration: int binDigits(const std::vector<int> &v);
// Example: binDigits(std::vector<int>{1, 0, 1, 1, 0}) => 22

// And now easier ones

// Task E1
// Check whether the number is even
// Declaration: bool isEven(int n);
// Example: isEven(13) => false;
bool isEven(int n) {
	return !(n & 1);
}

// Task E2
// Get the length of a byte string [reimplement strlen()]
// Declaration: int strlen(const char* str);
// Example: strlen("abcdef") => 6;
int strlen(const char* str) {
	int len = 0;
	while (str[++len]);
	return len;
}

// Task E3
// Count the number of spaces in std::string
// Declaration: int countSpaces(const std::string& str);
// Example: countSpaces("He ll o, Wo rl d!") => 5;
int countSpaces(const std::string& str) {
	int numSpaces = 0;
	for (auto elem : str) {
		if (elem == ' ')
			++numSpaces;
	}
	return numSpaces;
}

// Task E4
// Calculate the sum of digits of a non-negative number
// Declaration: int sumDigits(unsigned int n);
// Example: sumDigits(256) => 13;
int sumDigits(unsigned int n) {
	int sum = 0;
	while (n > 0){
		sum += n % 10; // Get the last digit
		n /= 10; // Remove the last digit
	}
	return sum;
}

// Task E5
// Encrypt a string using Caesar cipher with a given shift
// Declaration: std::string encryptCaesar(const std::string& str, int lshift=1);
// Example: encryptCaesar("Hello") => "Ifmmp";
// Example: encryptCaesar("World of Warcraft.", -2) => "Umpjb md Uypapydr.";
std::string encryptCaesar(const std::string& str, int lshift=1) {
	std::string encString{str};
	for (auto& elem : encString) {
		if (elem > 64 && elem <= 90) { // uppercase letters
			elem = ((elem - 65) + lshift) % 26 + 65;
			if (elem < 65)
				elem += 26;
		}
		if (elem > 96 && elem <= 122) { // lowercase letters
			elem = ((elem - 97) + lshift) % 26 + 97;
			if (elem < 97)
				elem += 26;
		}
	}
	return encString;
}

int main(int argc, char const *argv[]) {
	std::string s {"aeub"};
	std::cout << isEven(12) << std::endl;
	std::cout << strlen("abcdef") << std::endl;
	std::cout << countSpaces("He ll o, Wo rl d!") << std::endl;
	std::cout << sumDigits(256) << std::endl;
	std::cout << encryptCaesar("Hello") << std::endl;
	std::cout << encryptCaesar("World of Warcraft.", -2) << std::endl;

	return 0;
}
