/***************************
 * Practice: DATE AND TIME *
 * Date:     2021-04-12    *
 ***************************/

#include <string>
#include <ctime>
#include <iostream>
#include <iomanip>

int main(int argc, char* argv[]) {

	// Task 1
	// A time string is passed as a command line argument
	// in ISO 8601 format (e.g. "2021-04-11T19:54:10Z")
	// Print out the number of hours between now and the given time
	// Expect valid input
	
	std::string ts = argv[1];
	std::tm givenTime;
	givenTime.tm_year = std::stoi(ts.substr(0, 4)) - 1900;
	givenTime.tm_mon = std::stoi(ts.substr(5, 2)) - 1;
	givenTime.tm_mday = std::stoi(ts.substr(8, 2));
	givenTime.tm_hour = std::stoi(ts.substr(11, 2));
	givenTime.tm_min = std::stoi(ts.substr(14, 2));
	givenTime.tm_sec = std::stoi(ts.substr(17, 2));
	std::time_t givenTimestamp = std::mktime(&givenTime);
	std::time_t curTimestamp = std::time(nullptr);

	std::cout << "The given time is "
	          << std::difftime(givenTimestamp, curTimestamp) / 3600.
	          << " hours away from now\n";

	// Task 2
	// A timestamp is passed as a command line argument
	// Print out the date in format "Sunday of week 14"
	// Expect valid input
	
	std::time_t anotherTimestamp = std::atol(argv[2]);
	std::tm* anotherTime = std::gmtime(&anotherTimestamp);
	std::cout << anotherTimestamp << " : "
	          << std::put_time(anotherTime, "%A of week %V%n");

	// Task 3
	// Print out the CPU time used to execute your program
	
	double out = 0.;
	for (int i = 0; i < 10000; ++i)
		for (int j = 0; j < 100000; ++j)
			out += i * j / 1.e12;
	std::cout << "Used CPU time: " << 1.*std::clock() / CLOCKS_PER_SEC << " s\n";
	return 0;
}