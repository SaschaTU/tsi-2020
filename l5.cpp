/************************
 * Practice: FILES      *
 * Date:     2021-03-24 *
 ************************/

// Task
// Create a database table with the current weather.
// Description:
// +----------------------------------------+
// | Weather                                |
// +-----------------------+----------------+
// | Location              | char[20]       |
// | Temperature (°C)      | float          |
// | Pressure (hPa)        | unsigned short |
// | Speed of wind (m/s)   | unsigned short |
// | Direction of wind (°) | unsigned short |
// +-----------------------+----------------+
// Implementation: binary file
// Functionality:
// -- Read in data
// -- Add record
// -- Output records
// -- Write out data

#include <iostream>
#include <fstream>
#include <vector>

struct Weather {
	char location[20];
	float temperature;
	unsigned short pressure;
	unsigned short speed;
	unsigned short direction;
};

std::ostream& operator<<(std::ostream& os, Weather& w) {
	os << w.location << " | "
	   << w.temperature << " °C | "
	   << w.pressure << " hPa | "
	   << w.speed << " m/s (" << w.direction << "°)";
	return os;
}

int main(int argc, char* argv[]) {
	// Weather w{"Riga, LV", 4.5, 1002, 1, 270};
	// std::cout << w << std::endl;
	// w.direction = 180;
	// std::cout << w << std::endl;

	char fname[]{"dbWeather"};
	std::vector<Weather> ws;

	// Read in data
	if (std::ifstream is{fname, std::ios::binary|std::ios::ate}) {
		auto flen = is.tellg();
		auto size = flen / sizeof(Weather);
		is.seekg(0);
		for (int i = 0; i < size; ++i) {
			Weather tmp;
			is.read(reinterpret_cast<char*>(&tmp), sizeof tmp);
			ws.push_back(tmp);
		}
		std::cout << "Read " << size << " record(s) from file " << fname << "\n";
	} else {
		std::cout << "File " << fname << " does not exist. Assuming new table.\n";
	}

	// Output records
	std::cout << "\n######## Weather ########\n";
	for (auto& e : ws) {
		std::cout << e << std::endl;
	}

	// Add record
	Weather* w1 = new Weather;
	std::cout << "\n######## New record ########\n";
	std::cout << "Location: ";
	std::cin >> w1->location;
	std::cout << "Temperature (°C): ";
	std::cin >> w1->temperature;
	std::cout << "Pressure (hPa): ";
	std::cin >> w1->pressure;
	std::cout << "Wind speed (m/s): ";
	std::cin >> w1->speed;
	std::cout << "Wind direction (°): ";
	std::cin >> w1->direction;
	std::cout << std::endl;
	ws.push_back(*w1);
	delete w1;

	// Write out data
	if (std::ofstream os{fname, std::ios::binary}) {
		for (auto& e : ws) {
			os.write(reinterpret_cast<char*>(&e), sizeof e);
		}
		std::cout << "Wrote " << ws.size() << " record(s) to file " << fname << "\n";
	}

	return 0;
}
