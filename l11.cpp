/************************
 * Practice: REGEXPS    *
 * Date:     2021-04-21 *
 ************************/

// Task
// Use regular expressions to validate the input:
// -- Id should be integer
// -- Code should start with 'M'
//    and be formatted like 'M-000-X'
// Code should be processed into an integer

#include <iostream>
#include <string>
#include <regex>

struct Data {
	int id;
	int code;
};

int fillId() {
	std::string inId;
	std::regex valId("[0-9]+");
	while (true) {
		std::cout << "Id: ";
		std::getline(std::cin, inId);
		if (std::regex_match(inId, valId))
			break;
		else
			std::cerr << "EEEE PosInt expected\n";
	}
	return std::stoi(inId);
}

int fillCode() {
	std::string inCode;
	std::regex valCode("M-([0-9]+)-[A-Z]"); // M-000-X
	std::smatch matchCode;
	while (true) {
		std::cout << "Code: ";
		std::getline(std::cin, inCode);
		if (std::regex_match(inCode, matchCode, valCode))
			break;
		else
			std::cerr << "EEEE M-000-X format expected\n";
	}
	return std::stoi(matchCode[1]); // ...([0-9]+)...
}

int main(int argc, char* argv[]) {
	Data d;
	d.id = fillId();
	d.code = fillCode();
	std::cout << "{\"Id\": " << d.id << ", \"Code\": \"" << d.code << "\"}\n";
	return 0;
}